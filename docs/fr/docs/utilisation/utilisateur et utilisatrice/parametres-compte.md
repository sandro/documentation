# Les paramètres de votre compte

Vous pouvez accéder à vos paramètres en cliquant sur votre avatar puis sur **Mon compte**.

## Général

### Email

Vous pouvez modifier l'email que vous utilisez pour vous connecter à votre compte. Pour cela, vous devez le faire :

  1. cliquez sur votre avatar dans la barre supérieure
  * cliquez sur **Mon compte**
  * cliquez sur l'onglet **Général** dans la barre de gauche
  * entrez votre nouvel e-mail dans le champ **Nouvelle adresse e-mail**
  * entrez votre mot de passe dans le champ **Mot de passe**
  * cliquez sur le bouton **Changer mon adresse e-mail**.

!!! note
    Vous recevrez un email de confirmation.

### Mot de passe

  1. cliquez sur votre avatar dans la barre supérieure
  * cliquez sur **Mon compte**
  * cliquez sur l'onglet **Général** dans la barre de gauche
  * entrer votre **ancien** mot de passe (celui à changer) dans le champ **Ancien mot de passe**
  * entrer le nouveau dans le champ **Nouveau mot de passe**
  * cliquer sur **Modifier mon mot de passe**

### Suppression du compte

!!! danger
    Vous allez tout perdre. Les identités, les profils, les paramètres, les événements créés, les messages et les participations auront disparu à jamais.
    **Il n'y aura aucun moyen de récupérer vos données**.

Pour supprimer vos données vous devez&nbsp;:

  1. cliquez sur votre avatar dans la barre supérieure
  * cliquez sur **Mon compte**
  * cliquez sur l'onglet **Général** dans la barre de gauche
  * cliquer sur le bouton **Supprimer mon compte**
  * entrer votre mot de passe pour confirmer votre action
  * cliquer sur le bouton **Tout supprimer** (ou sur le bouton **Annuler** pour garder votre compte)

## Préférences

Vous pouvez modifier la langue de l'interface de votre compte et le fuseau horaire. Pour faire cela vous devez&nbsp;:

  1. cliquez sur votre avatar dans la barre supérieure
  * cliquez sur **Mon compte**
  * cliquez sur **Préférences** dans le menu latéral gauche
  * changer la langue ou/et le fuseau horaire

Vous pouvez aussi indiquer une ville ou région, ainsi qu'un rayon pour avoir une suggestion d'événements près de chez vous.

## Notifications

Vous avez la possibilité de recevoir (ou non) des notifications pour diverses activités.

  1. cliquez sur votre avatar dans la barre supérieure
  * cliquez sur **Mon compte**
  * cliquez sur l'onglet **Notifications** dans la barre de gauche&nbsp;:


### Notifications du navigateur

Grâce aux notifications push, vous pouvez afficher dans votre navigateur les notifications les plus importantes de l'activité de votre compte sur Mobilizon. Vous n'avez même pas besoin d'être connectée à Mobilizon pour que ces notifications apparaissent. Pour activer ce système de notifications, cliquez tout simplement sur le bouton **<svg style="width:24px;height:24px" viewBox="0 -5 24 24">
    <path fill="currentColor" d="M6.18,15.64A2.18,2.18 0 0,1 8.36,17.82C8.36,19 7.38,20 6.18,20C5,20 4,19 4,17.82A2.18,2.18 0 0,1 6.18,15.64M4,4.44A15.56,15.56 0 0,1 19.56,20H16.73A12.73,12.73 0 0,0 4,7.27V4.44M4,10.1A9.9,9.9 0 0,1 13.9,20H11.07A7.07,7.07 0 0,0 4,12.93V10.1Z" />
</svg> Activer les notifications push du navigateur**.

!!! note
    Une fenêtre vous demandant si vous souhaitez autoriser Mobilizon à vous envoyer des notifications apparaît. Cliquez sur autoriser.

### Paramètres des notifications

Dans cette partie, vous pouvez cocher / décocher les notifications que vous souhaitez avoir par mail et / ou en notifications push dans votre navigateur.

![captures montrant les différentes possibilités de notifications](../../images/mobilizon-notifications-check-FR.png)

Si vous avez coché la réception de notifications par mail, vous pouvez choisir la fréquence de l'envoi de ce mail dans le champ **Envoyer des e-mails de notification**&nbsp;:

  * Ne pas recevoir d'e-mail
  * Recevoir un e-mail à chaque activité
  * E-mail récapitulatif chaque heure
  * E-mail récapitulatif chaque jour
  * Résumé hebdomadaire par e-mail

### Notifications de participation

Par défaut, Mobilizon vous envoie systématiquement un email lors de changements importants pour les événements auxquels vous participez&nbsp;: date et heure, adresse, confirmation ou annulation, etc. Mais vous avez aussi la possibilité de demander à recevoir un mail de rappel chaque jour où ont lieu des évènements auxquels vous avez indiqué votre participation, une heure avant le début de chaque évènement ou à recevoir chaque lundi un récapitulatif des évènements de la semaine.

![section participations](../../images/notifications-participations-FR.png)

### Notifications pour organisateur·ice

En tant qu'organisateur·ice, vous pouvez recevoir des notifications pour les participations à un événement nécessitant une approbation manuelle. Vous pouvez choisir entre&nbsp;:

  * Ne pas recevoir d'e-mail
  * Recevoir un e-mail par demande
  * E-mail récapitulatif chaque heure
  * E-mail récapitulatif chaque jour
  * Résumé hebdomadaire par e-mail

## Flux personnels

 Ces flux RSS/Atom et ICS/WebCal contiennent des informations sur les événements pour lesquels n'importe lequel de vos profils est un⋅e participant⋅e ou un⋅e créateur⋅ice. Vous devriez les garder privés. Vous pouvez trouver des flux spécifiques à chaque profil sur la page d'édition des profils.
