# Federation with other Instances

!!! info
    You have to be **administrator** to access this section

You can manually add other Mobilizon instances to  federate with.
Admins of the other instance will need to approve the follow request from your instance, before federation is enabled.

## Limitations
- Federation fully works only between Mobilizon instances.
  Other software supporting ActivityPub may work, eg you can follow a Mobilizon user from a Mastodon account.

- A subscription is one-way; for example A chooses to subscribe to B, this does not mean that B contains events from A.
  If A subscribes to B, A will have all its own events AND the public events of B.

- Only the content modified after the subscription is actually transmitted. If A subscribes to B, B's old events are not on A.

- Only public content is transmitted (by definition! otherwise private content would not be private).
  If A subscribes to B, A does not see B's private events.
  If you are a user on B and authorized to see certain private content, you see more of it than by going to A.
