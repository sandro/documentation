# How to deal with moderation

!!! info
    You have to be **moderator** or **administrator** to access this section

This section aims to explain how to deal with [comment](../events/comment-event.md#report-a-comment) and [event](../events/event-action.md#report) reports.

## Reports view

!!! note
    When someone reports a comment or event, moderators and administrators receive a notification email.

Reports are available by:

  1. clicking your avatar
  * clicking **Administration**
  * clicking **Reports** in the **Moderation** section in the left sidebar menu

You can filter reports by their **Open**, **Resolved** and **Closed** status:

![new report view](../../images/en/moderator-reports-list-EN.png)

### Reported comments

When a new report is available, you can click on it to open the actions view:

![report comments actions view annoted](../../images/en/moderator-report-comment-view-annoted-EN.png)

  1. **Mark as resolved** button: report accepted and processed
  * **Close** button: report rejected
  * Delete reported **event**
  * Comment filled by the account **that reported** the comment
  * Name of the **reported account** and comment reported
  * Delete the **reported comment**

### Reported events

When a new report is available, you can click on it to open the actions view:

![report events actions view annoted](../../images/en/moderator-report-event-view-annoted-EN.png)

  1. **Mark as resolved** button: report accepted and processed
  * **Close** button: report rejected
  * Comment filled by the account **that reported** the event
  * The event that has been reported
  * Delete the event

### Moderation discussion

Moderators can discuss a report by using the **Notes** section:

![report notes](../../images/en/report-notes.png)

## Moderation log

This section lists the actions that have been taken by the moderators.
