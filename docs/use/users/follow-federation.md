# Follow activities through the federation

## With Mastodon/Pleroma

### Follow a group via Mastodon/Pleroma

You have the ability to view the activity of a group on a Mobilizon instance in your Mastodon/Pleroma feed. To do this you need to have an account on the Mastodon/Pleroma network, and then:

  1. search for the mobilizon group handle in Mastodon/Pleroma searchbar
  1. click the icon to follow the group

![registration page image](../../images/en/mastodon-follow-group1.png)

Or:

  1. search for the mobilizon group handle in Mastodon/Pleroma searchbar
  1. click the account name
  1. click the **Follow** button

![registration page image](../../images/en/mastodon-follow-group.gif)

!!! note
    An administrator account in the group can [reject a follow request](../groups/group-manage-followers.md#reject-a-follower-subscribe). If the **Follow** button reappears, your request may have been rejected.
